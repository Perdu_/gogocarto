#!/bin/bash
HOST=${HOST:-backup.host}
USER=${USER:-user}
PASSWORD=${PASSWORD:-pass}
KEEPBACKUPDAYS=${KEEPBACKUPDAYS:-10}
DIRECTORYMONGO=${DIRECTORYMONGO:-/var/backups/mongobackups}
DIRECTORYFILES=${DIRECTORYFILES:-/var/backups/gogocarto-uploads}
GOGOCARTODIRECTORY=${GOGOCARTODIRECTORY:-/var/www/gogocarto}
LOG=$DIRECTORYMONGO/backup.log
TODAY=`date +"%Y-%m-%d"`

if [ ! -d "$DIRECTORYMONGO" ]; then
  mkdir -p $DIRECTORYMONGO
fi

if [ ! -d "$DIRECTORYFILES" ]; then
  mkdir -p $DIRECTORYFILES
fi

printf "\n\n" >> $LOG

# backup mongodb
echo $(date) "Starting mongo dump" >> $LOG
mongodump --out $DIRECTORYMONGO/$TODAY

# backup gogocarto files
echo $(date) "Starting backup upload" >> $LOG
mkdir -p $DIRECTORYFILES/$TODAY
tar -zcvf $DIRECTORYFILES/$TODAY/uploads.tar.gz $GOGOCARTODIRECTORY/web/uploads

# clean older backups
echo $(date) "Clean old backups" >> $LOG
find $DIRECTORYFILES/ -mtime +$KEEPBACKUPDAYS -exec rm -rf {} \;
find $DIRECTORYMONGO/ -mtime +$KEEPBACKUPDAYS -exec rm -rf {} \;

# compress and remplace existing on ftp
echo $(date) "Compress" >> $LOG
tar -zcvf $DIRECTORYFILES/$TODAY/mongo.tar.gz $DIRECTORYMONGO/$TODAY

cd $DIRECTORYFILES/$TODAY
echo $(date) "Send to FTP" >> $LOG
lftp -u $USER,$PASSWORD $HOST -e "mkdir -p gogocarto/$TODAY;rm -rf gogocarto/`date --date='11 day ago' +%Y-%m-%d`;cd gogocarto/$TODAY;put uploads.tar.gz; put mongo.tar.gz; exit"
# remove local database tarball, we keep the unzip one
rm $DIRECTORYFILES/`date +"%Y-%m-%d"`/mongo.tar.gz
